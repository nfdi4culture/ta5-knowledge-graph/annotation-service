/**
 * MapStruct mappers for mapping domain objects and Data Transfer Objects.
 */
package org.tib.osl.annotationservice.service.mapper;
